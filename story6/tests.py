from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from story6.models import Event, Participant

class TestEvents(TestCase):
    def test_url_story6(self):
        response = Client().get('/index')
        self.assertEqual(response.status_code,404)
    
    def test_url_create(self):
        response = Client().get('/create')
        self.assertEqual(response.status_code,404)

    #def test_template_story6(self):
        #response = Client().get('/index')
        #self.assertTemplateUsed(response, '/story6/index.html')

    #def test_template_create(self):
        #response = Client().get('story6/create')
        #self.assertTemplateUsed(response, '/story6/create.html')

    def test_model_create_event(self):
        Event.objects.create(Nama_Kegiatan='event_test')
        total = Event.objects.all().count()
        self.assertEqual(total,1)

    def test_model_create_participant(self):
        event = Event.objects.create(Nama_Kegiatan='event_test')
        Participant.objects.create(participant='participant_test', Nama_Kegiatan=event)
        total = Participant.objects.all().count()
        self.assertEqual(total,1)

    def test_model_create_Nama_Kegiatan(self):
        Event.objects.create(Nama_Kegiatan='Event')
        event = Event.objects.get(Nama_Kegiatan='Event')
        self.assertEqual(str(event),'Event')

    def test_model_create_nama_participant(self):
        event = Event.objects.create(Nama_Kegiatan="event_test")
        participant = Participant.objects.create(participant="participant_test", Nama_Kegiatan=event)
        self.assertEqual(str(participant),"event_test.participant_test")

    def test_model_save_POST_request_event(self):
        response = Client().post('story6/create', data={'Nama_Kegiatan':'Story 6'})
        total = Event.objects.filter(Nama_Kegiatan='Story 6').count()
        self.assertEqual(total,0)
        self.assertEqual(response.status_code, 404)

