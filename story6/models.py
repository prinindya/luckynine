from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Event(models.Model):
    Nama_Kegiatan = models.CharField(max_length=200,  blank=True)

    def __str__(self):
        return "{}".format(self.Nama_Kegiatan)

class Participant(models.Model):
    Nama_Kegiatan = models.ForeignKey(Event, on_delete=models.CASCADE)
    participant = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return "{}.{}".format(self.Nama_Kegiatan, self.participant)

