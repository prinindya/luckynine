from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
# from .forms import PostForm
# from .models import PostModel

from .forms import EventForm, ParticipantForm
from .models import Event, Participant
def index(request):
    Nama_Kegiatan = Event.objects.all()
    event = []
    for nama in Nama_Kegiatan:
        participant = Participant.objects.filter(Nama_Kegiatan_id= nama)
        participant_list =[]
        for peserta in participant:
            participant_list.append(peserta)
        event.append((nama, participant_list))
    
    
    participant_form = ParticipantForm(request.POST)
    if request.method == 'POST':
        if 'participant' in request.POST and 'id_Nama_Kegiatan' in request.POST:
            if participant_form.is_valid():
                event = Event.objects.get(id=request.POST['id_Nama_Kegiatan'])                
                data = participant_form.cleaned_data
                data_event = Participant()
                data_event.participant = data['participant']
                data_event.Nama_Kegiatan = event
                data_event.save()
                return HttpResponseRedirect("/story6/index")
    context = {
        'event' : event,
        'participant_form' : participant_form,
    }

    return render(request, 'index.html', context)

def create_event(request):
    event_form = EventForm(request.POST)
    if request.method == 'POST':
        if 'Nama_Kegiatan' in request.POST:
            if event_form.is_valid():
                event_form.save()
                return HttpResponseRedirect("/story6/index")

    context = {
        'event_form' : event_form,
    }
    return render(request, 'create.html', context)
