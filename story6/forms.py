from django import forms

#import model dari models.py
from .models import Event, Participant

class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            'Nama_Kegiatan',
        ]
        widgets = {
            'Nama_Kegiatan': forms.TextInput(  
                attrs={
                    'class' : 'form-control',
                }
            ),
        }

class ParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = [
            'participant',
        ]
        widgets = {
            'participant': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                }
            ),
        }

        
