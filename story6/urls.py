from django.urls import path

from . import views

app_name = 'story6'


urlpatterns = [
    path('create', views.create_event),
    path('index', views.index),
]
