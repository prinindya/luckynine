from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    #path('index', views.indexView),
    path('dashboard', views.dashboardView, name='dashboard'),
    path('home', views.indexView),
    path('register', views.registerView, name='register_url'),
    path('login', views.loginView, name="login_url"),
    path('logout', views.logoutView, name='logout'),
]
