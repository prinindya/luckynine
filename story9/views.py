from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.contrib import messages
# Create your views here.

from .forms import CreateUserForm

def indexView(request):
    return render(request, 'home.html')

@login_required
def dashboardView(request):
    return render(request, 'dashboard.html')

def registerView(request):
    if request.user.is_authenticated:
        return redirect('story9:home')
    else:
        form = CreateUserForm()
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('story9:login_url')

        return render(request, 'register.html', {'form': form})

def loginView(request):
    if request.user.is_authenticated:
    	return redirect('story9:dashboard')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username = username, password = password)

            if user is not None:
                login(request, user)
                return redirect('story9:dashboard')
            else:
                messages.error(request, 'Username OR password is incorrect')

        return render(request, 'login.html')

def logoutView(request):
    logout(request)
    return redirect('story9:login_url')
