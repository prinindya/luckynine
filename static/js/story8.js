$(document).ready(() => {
    get_books('harry');

    $('#search-button').on('click', event => {
        var keyword = $('#search-box').val();
        if(keyword == '') keyword = 'disney';
        get_books(keyword);
    });

    function get_books(keyword) {
        $('#main-table').hide();
        $.ajax({
            type: "GET",
            url: get_books_url + keyword,
            success: result => {
                if(!result.items) {
                    alert("No Result Found!");
                } else {
                    $('.data').empty();
                    result.items.forEach(item => {
                        const book = {
                            id: item.id,
                            cover: () => {
                                if(item.volumeInfo.imageLinks) {
                                    return item.volumeInfo.imageLinks.thumbnail;
                                }
                                return default_cover;
                            },
                            title: item.volumeInfo.title,
                            author: () => {
                                if(item.volumeInfo.authors) {
                                    var str = "";
                                    item.volumeInfo.authors.forEach(author => {
                                        str += ', ' + author
                                    });
                                    return str.slice(2);
                                } 
                                else return "unknown";
                            },
                            publishedDate: (item.volumeInfo.publishedDate ||'Publish date is not available'),
                            publisher: (item.volumeInfo.publisher || "Publisher is unknown"),
                        }
                        $('#main-table').append(
                            $('<tr/>').addClass('data').attr('id', book.id).append(
                                $('<td/>').addClass('cover').append(
                                    $('<img/>').attr('src', book.cover).addClass('book-cover')
                                )
                            ).append(
                                $('<td/>').addClass('title').html(book.title)
                            ).append(
                                $('<td/>').addClass('author').html(book.author)
                            ).append(
                                $('<td/>').addClass('publishedDate').html(book.publishedDate)
                            ).append(
                                $('<td/>').addClass('publisher').html(book.publisher)
                            )
                        );
                    });
                }
                $('#main-table').show();
            }
        });
    }
});
