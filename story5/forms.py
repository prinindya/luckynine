from django import forms

#import model dari models.py
from .models import PostModel

class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = [
            'Mata_Kuliah',
            'SKS',
            'Ruangan',
            'Pengajar',
            'semester',
            'Keterangan',            
        ]
        widgets = {
            'Mata_Kuliah': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                    'placeholder' : 'Tuliskan nama Mata kuliah',
                }
            ),
            'SKS': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                }
            ),
            'Ruangan': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                }
            ),
            'Pengajar': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                    'placeholder' : 'Tuliskan nama dosen anda',
                }
            ),
            'semester': forms.Select(  
                attrs={
                    'class' : 'form-control', 
                }
            ),           
            'Keterangan': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                    'placeholder' : 'Catatan tambahan',
                }
            ),
        }
# 'class' : 'form-control' digunakan untuk mengisi sesuai bootstrap
