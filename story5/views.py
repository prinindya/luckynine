from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import PostForm
from .models import PostModel

def delete(request, delete_id):
    PostModel.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("/story5/list")

def update(request, update_id):
    akun_update = PostModel.objects.get(id=update_id)
    data = {
        'Mata_Kuliah' : akun_update.Mata_Kuliah,
        'SKS' : akun_update.SKS,
        'Ruangan' : akun_update.Ruangan,
        'Pengajar' : akun_update.Pengajar,
        'semester' : akun_update.semester,
        'Keterangan': akun_update.Keterangan,
    }
    post_form = PostForm(request.POST or None, initial=data, instance=akun_update)

    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/story5/list")

    
    context = {
        'pageTitle' : 'Update Form',
        'post_form' : post_form,
    }
    return render(request, 'story5/create.html', context)

def index(request):
    posts = PostModel.objects.all()
    context = {
        'heading' : 'Post',
        'contents': 'List Post',
        'posts' : posts,
    }
    return render(request, 'list.html', context)

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/story5/list")

    
    context = {
        'pageTitle' : 'Create Form',
        'post_form' : post_form,
    }
    return render(request, 'create.html', context)
