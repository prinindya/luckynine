from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('create', views.create, name='create'),
    path('list', views.index),
    path('update/<int:update_id>', views.update, name='update'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
]