from django.db import models
from django.core.exceptions import ValidationError
# Create your models here.
# memvalidasi di model krn yg ingin di validasi model 

def validate_author(value):
    judul_input = value
    if judul_input == "Einstein":
        message = "maaf" + judul_input + "tidak bisa diposting"
        raise ValidationError(message)

class PostModel(models.Model):
    Mata_Kuliah = models.CharField(max_length=100)
    Keterangan = models.TextField(
        max_length=300,
        validators= [validate_author]
        )
    Pengajar = models.CharField(max_length=100)
    Ruangan = models.CharField(max_length=100)
    SKS = models.CharField(max_length=100)

    LIST_SEMESTER = (
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021'),
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2018/2019', 'Gasal 2018/2019'),
        ('Genap 2018/2019', 'Genap 2018/2019'),
        ('Gasal 2017/2018', 'Gasal 2017/2018'),
        ('Genap 2017/2018', 'Genap 2017/2018'),
        )
    semester = models.CharField(
        max_length=100,
        choices= LIST_SEMESTER,
        default  = 'Maung',
    )
    published = models.DateTimeField(auto_now_add=True)
    
def __str__(self):
    return "{}.{}".format(self.id, self.judul)