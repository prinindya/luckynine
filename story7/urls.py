from django.urls import path

from . import views

app_name = 'story7'


urlpatterns = [
    path('story7', views.index),
    path('story8', views.story8),
    path('getBooks', views.getBooks, name='getBooks'),
]