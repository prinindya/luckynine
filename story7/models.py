from django.db import models

# Create your models here.

class Book(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    cover = models.URLField(max_length=400)
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200, blank=True)
    publishedDate = models.DateField(max_length=100, blank=True)
    publisher = models.CharField(max_length=200, blank=True)
