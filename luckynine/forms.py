from django import forms

class FormField(forms.Form):
    #python data type
    integer_field = forms.IntegerField(required = False)
    decimal_field = forms.DecimalField(required = False)
    float_field = forms.FloatField(required = False)
    boolean_field = forms.BooleanField(required = False)
    char_field = forms.CharField(max_length=100)

    #string input
    email_field = forms.EmailField() #khusu buat isi email jd hrs ada @
    slug_field = forms.SlugField()
    url_field = forms.URLField()
    ip_field = forms.GenericIPAddressField()

    #select input
    PILIHAN = (
        ('nilai1', 'Pilihan1'),
        ('nilai2', 'Pilihan2'),
        ('nilai3', 'Pilihan3'),

    )
    choice_field = forms.ChoiceField(choices=PILIHAN) #make tuple/list
    multi_chocie_field = forms.ChoiceField(choices=PILIHAN)
    multi_typed_field = forms.TypedMultipleChoiceField(choices=PILIHAN)


    #source https://docs.djangoproject.com/en/3.1/topics/forms/
